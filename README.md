# Na'vi notes
My notes from Trilium, exported and hosted at https://ictman1076.gitlab.io/navi-notes/

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# Importing into Trilium
1. Clone or [download](https://gitlab.com/ICTman1076/navi-notes/-/archive/master/navi-notes-master.zip)
 the repo. If it's a ZIP file, extract it.
2. Take the `public` directory and rename it to something sensible - I call mine `Na'vi`.
3. Compress that into a ZIP file.
4. Right click the note you want Na'vi to be a child of, click `Import into note`.
5. Select the ZIP file you just made, and leave the rest of the settings.

You're done :)

# Contributing
Unless you're correcting a mustake, please don't. I want to make my notes myself. That's not to say
you can't adapt these notes for yourself, feel free to! In fact, 
[download Trilium](https://github.com/zadam/trilium/releases/), and follow the guide above to import it for
yourself.

